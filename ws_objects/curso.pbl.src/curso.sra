﻿$PBExportHeader$curso.sra
$PBExportComments$Generated Application Object
forward
global type curso from application
end type
global uo_trans sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
string gs_usuario
string is_clienttype

end variables

global type curso from application
string appname = "curso"
string themepath = "C:\Program Files (x86)\Appeon\Shared\PowerBuilder\theme190"
string themename = "Flat Design Dark"
long richtextedittype = 2
long richtexteditversion = 1
string richtexteditkey = ""
end type
global curso curso

on curso.create
appname="curso"
message=create message
sqlca=create uo_trans
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on curso.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//messagebox("AVISO","RUN APP")
open(w_logon)
end event

event close;disconnect using sqlca;
end event

