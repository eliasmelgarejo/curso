﻿$PBExportHeader$uo_dw_base.sru
forward
global type uo_dw_base from datawindow
end type
end forward

global type uo_dw_base from datawindow
integer width = 2382
integer height = 1220
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event ue_config_dw ( )
end type
global uo_dw_base uo_dw_base

type variables
boolean ib_autoretrieve = false
boolean ib_autoinsert = false

end variables

event ue_config_dw();if this.ib_autoretrieve then 
	this.retrieve()
end if

if this.ib_autoinsert then
	this.insertrow(0)
end if
end event

on uo_dw_base.create
end on

on uo_dw_base.destroy
end on

event constructor;This.settransobject( sqlca)


this.postevent( "ue_config_dw")
//this.event post ue_config_dw( )

//this.triggerevent( "ue_config_dw")
//this.event ue_config()

//messagebox("EE","")

end event

event dberror;Messagebox("ERROR EN BD" + string(sqldbcode) ,sqlerrtext)
end event

