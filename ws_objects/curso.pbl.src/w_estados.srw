﻿$PBExportHeader$w_estados.srw
forward
global type w_estados from w_padre
end type
type dw_datos from datawindow within w_estados
end type
type cb_5 from uo_boton within w_estados
end type
type cb_4 from uo_boton within w_estados
end type
type cb_3 from uo_boton within w_estados
end type
type cb_guardar from uo_boton within w_estados
end type
type cb_1 from uo_boton within w_estados
end type
end forward

global type w_estados from w_padre
integer width = 3721
integer height = 2348
string title = "Estados"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_datos dw_datos
cb_5 cb_5
cb_4 cb_4
cb_3 cb_3
cb_guardar cb_guardar
cb_1 cb_1
end type
global w_estados w_estados

on w_estados.create
int iCurrent
call super::create
this.dw_datos=create dw_datos
this.cb_5=create cb_5
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_guardar=create cb_guardar
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_datos
this.Control[iCurrent+2]=this.cb_5
this.Control[iCurrent+3]=this.cb_4
this.Control[iCurrent+4]=this.cb_3
this.Control[iCurrent+5]=this.cb_guardar
this.Control[iCurrent+6]=this.cb_1
end on

on w_estados.destroy
call super::destroy
destroy(this.dw_datos)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_guardar)
destroy(this.cb_1)
end on

event open;call super::open;dw_datos.settransobject(sqlca)
end event

type dw_datos from datawindow within w_estados
integer x = 151
integer y = 48
integer width = 2798
integer height = 2184
integer taborder = 20
string title = "none"
string dataobject = "dw_estados"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_5 from uo_boton within w_estados
integer x = 3067
integer y = 492
integer taborder = 40
string text = "Borrar"
end type

event clicked;call super::clicked;integer li_fila
li_fila = dw_datos.getrow()
dw_datos.deleterow(li_fila)

cb_guardar.event clicked() //llama al boton guardar 

end event

type cb_4 from uo_boton within w_estados
integer x = 3067
integer y = 352
integer taborder = 30
string text = "Insertar"
end type

event clicked;call super::clicked;dw_datos.insertrow(0)
dw_datos.scrolltorow(dw_datos.rowcount())
dw_datos.setrow(dw_datos.rowcount())

end event

type cb_3 from uo_boton within w_estados
integer x = 3067
integer y = 632
integer taborder = 30
string text = "Salir"
end type

event clicked;call super::clicked;close(parent)
end event

type cb_guardar from uo_boton within w_estados
integer x = 3067
integer y = 212
integer taborder = 20
string text = "Guardar"
end type

event clicked;call super::clicked;integer li_ret

li_ret = dw_datos.update()

if li_ret = 1 then
	commit;
else 
	messagebox("ERROR",sqlca.sqlerrtext)
	rollback;
end if
	

end event

type cb_1 from uo_boton within w_estados
integer x = 3067
integer y = 48
integer taborder = 10
string text = "Leer"
end type

event clicked;call super::clicked;dw_datos.retrieve( )


end event

