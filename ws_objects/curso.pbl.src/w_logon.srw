﻿$PBExportHeader$w_logon.srw
forward
global type w_logon from w_padre
end type
type sle_pwd from singlelineedit within w_logon
end type
type sle_usuario from singlelineedit within w_logon
end type
type st_3 from statictext within w_logon
end type
type st_2 from statictext within w_logon
end type
type st_1 from statictext within w_logon
end type
type cb_cancel from uo_boton within w_logon
end type
type cb_acep from uo_boton within w_logon
end type
end forward

global type w_logon from w_padre
integer width = 3026
integer height = 1912
string title = "Lgon"
long backcolor = 16777215
sle_pwd sle_pwd
sle_usuario sle_usuario
st_3 st_3
st_2 st_2
st_1 st_1
cb_cancel cb_cancel
cb_acep cb_acep
end type
global w_logon w_logon

on w_logon.create
int iCurrent
call super::create
this.sle_pwd=create sle_pwd
this.sle_usuario=create sle_usuario
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.cb_cancel=create cb_cancel
this.cb_acep=create cb_acep
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_pwd
this.Control[iCurrent+2]=this.sle_usuario
this.Control[iCurrent+3]=this.st_3
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_cancel
this.Control[iCurrent+7]=this.cb_acep
end on

on w_logon.destroy
call super::destroy
destroy(this.sle_pwd)
destroy(this.sle_usuario)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_cancel)
destroy(this.cb_acep)
end on

type sle_pwd from singlelineedit within w_logon
integer x = 1010
integer y = 724
integer width = 517
integer height = 132
integer taborder = 20
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean password = true
textcase textcase = lower!
borderstyle borderstyle = stylelowered!
end type

type sle_usuario from singlelineedit within w_logon
integer x = 1010
integer y = 472
integer width = 517
integer height = 132
integer taborder = 10
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
textcase textcase = lower!
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_logon
integer x = 123
integer y = 768
integer width = 800
integer height = 128
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 33554431
string text = "PWD:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_logon
integer x = 123
integer y = 492
integer width = 800
integer height = 128
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 33554431
string text = "Usuario:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_logon
integer x = 398
integer y = 72
integer width = 1911
integer height = 212
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
boolean underline = true
long textcolor = 255
long backcolor = 33554431
string text = "LOGON APP"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_cancel from uo_boton within w_logon
integer x = 2331
integer y = 1584
integer taborder = 40
string text = "Cancelar"
end type

event clicked;close(parent)
end event

type cb_acep from uo_boton within w_logon
integer x = 325
integer y = 1568
integer taborder = 30
string text = "Aceptar"
end type

event clicked;call super::clicked;string ls_usuario, ls_pwd 

ls_usuario = sle_usuario.text
ls_pwd = sle_pwd.text

//if ls_usuario <> "ANDRES" then
//	messagebox("ERROR","USUARIO")
//	sle_usuario.setfocus()
//	return 
//end if
//
//if ls_pwd <> "ANDRES" then
//	sle_pwd.setfocus()
//	sle_pwd.text = ""
//	messagebox("ERROR","PWD")
//	return 
//end if

gs_usuario = ls_usuario 

if sqlca.uf_connect(ls_usuario , ls_pwd) =  -1  then
	messagebox("ERROR" , sqlca.sqlerrtext)
	return 
end if


open(w_menu)

close(parent)
end event

