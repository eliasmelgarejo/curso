﻿$PBExportHeader$w_menu.srw
forward
global type w_menu from w_padre
end type
type mdi_1 from mdiclient within w_menu
end type
end forward

global type w_menu from w_padre
integer width = 4942
integer height = 3276
string title = "Menu Principal "
string menuname = "m_principal"
windowtype windowtype = mdihelp!
long backcolor = 16777215
mdi_1 mdi_1
end type
global w_menu w_menu

on w_menu.create
int iCurrent
call super::create
if this.MenuName = "m_principal" then this.MenuID = create m_principal
this.mdi_1=create mdi_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mdi_1
end on

on w_menu.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
destroy(this.mdi_1)
end on

event open;call super::open;this.Setmicrohelp(gs_usuario)
end event

type mdi_1 from mdiclient within w_menu
long BackColor=268435456
end type

