﻿$PBExportHeader$w_ordenes.srw
forward
global type w_ordenes from w_padre
end type
type cb_4 from commandbutton within w_ordenes
end type
type cb_2 from commandbutton within w_ordenes
end type
type cb_3 from commandbutton within w_ordenes
end type
type cb_af from commandbutton within w_ordenes
end type
type cb_1 from commandbutton within w_ordenes
end type
type dw_det from uo_dw_base within w_ordenes
end type
type dw_cab from uo_dw_base within w_ordenes
end type
end forward

global type w_ordenes from w_padre
integer width = 3122
integer height = 1976
cb_4 cb_4
cb_2 cb_2
cb_3 cb_3
cb_af cb_af
cb_1 cb_1
dw_det dw_det
dw_cab dw_cab
end type
global w_ordenes w_ordenes

type variables
long il_id 
end variables

on w_ordenes.create
int iCurrent
call super::create
this.cb_4=create cb_4
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_af=create cb_af
this.cb_1=create cb_1
this.dw_det=create dw_det
this.dw_cab=create dw_cab
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_4
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.cb_af
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.dw_det
this.Control[iCurrent+7]=this.dw_cab
end on

on w_ordenes.destroy
call super::destroy
destroy(this.cb_4)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_af)
destroy(this.cb_1)
destroy(this.dw_det)
destroy(this.dw_cab)
end on

type cb_4 from commandbutton within w_ordenes
integer x = 2638
integer y = 168
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Borrar "
end type

event clicked;dw_cab.deleterow(1)

do while dw_det.rowcount ()  > 0 
	dw_det.deleterow(1)
loop

if dw_det.update() = 1 then 
	if dw_cab.update() =  1 then
		commit;
		dw_cab.insertrow(0)
	else 
		messagebox("ERROR EN CAB",sqlca.sqlerrtext)
		rollback;
	end if
else
	messagebox("ERROR EN DET",sqlca.sqlerrtext)
	rollback;
end if 

/*
if dw_det.update() <>1 then 
	messagebox("ERROR EN DET",sqlca.sqlerrtext)
	rollback;
	return 
end if

if dw_cab.update() =  1 then
	commit;
	dw_cab.insertrow(0)
else 
	messagebox("ERROR EN CAB",sqlca.sqlerrtext)
	rollback;
end if


	
end if
*/


end event

type cb_2 from commandbutton within w_ordenes
integer x = 2633
integer y = 36
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Guardar"
end type

event clicked;if dw_cab.update() = 1 then 
	if dw_det.update() =  1 then
		commit;
	else 
		messagebox("ERROR EN DETALLE",sqlca.sqlerrtext)
		rollback;
	end if
else
	messagebox("ERROR EN CAB",sqlca.sqlerrtext)
	rollback;
end if 
end event

type cb_3 from commandbutton within w_ordenes
integer x = 2327
integer y = 632
integer width = 82
integer height = 64
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "-"
end type

event clicked;integer li_fila 

li_fila = dw_det.getrow()

dw_det.deleterow(li_fila)

for li_fila  = 1 to dw_det.rowcount()
	dw_det.setitem(li_fila,"line_id",li_fila)
next 

end event

type cb_af from commandbutton within w_ordenes
integer x = 2409
integer y = 632
integer width = 91
integer height = 64
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "+"
end type

event clicked;integer li_fila
li_fila = dw_det.insertrow(0)
dw_det.setitem(li_fila,"id",il_id)
dw_det.setitem(li_fila,"line_id",li_fila)
dw_det.setrow(li_fila)

end event

type cb_1 from commandbutton within w_ordenes
integer x = 2560
integer y = 1712
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "salir"
end type

event clicked;close(parent)
end event

type dw_det from uo_dw_base within w_ordenes
integer x = 137
integer y = 620
integer taborder = 20
string dataobject = "dw_orden_detalle"
end type

type dw_cab from uo_dw_base within w_ordenes
integer x = 146
integer y = 24
integer height = 580
integer taborder = 10
string dataobject = "dw_ordenes"
boolean ib_autoinsert = true
end type

event itemchanged;call super::itemchanged;long ll_id

if dwo.name = "id" then
	ll_id = dec(data) 
	il_id = ll_id
	if this.retrieve(ll_id) = 1 then 
		dw_det.retrieve(ll_id)
	else 
		this.insertrow(0)
		this.setitem(1,1,ll_id)
		dw_Det.reset()
		cb_af.event clicked()
	end if
end if
end event

