﻿$PBExportHeader$w_peronas.srw
forward
global type w_peronas from w_padre
end type
type cb_2 from commandbutton within w_peronas
end type
type cb_1 from commandbutton within w_peronas
end type
type cb_insertar from uo_boton within w_peronas
end type
type dw_datos from uo_dw_base within w_peronas
end type
type dw_lista from uo_dw_base within w_peronas
end type
type cb_3 from uo_boton within w_peronas
end type
type cb_guardar from uo_boton within w_peronas
end type
end forward

global type w_peronas from w_padre
integer width = 2743
integer height = 2076
string title = "Personas"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_2 cb_2
cb_1 cb_1
cb_insertar cb_insertar
dw_datos dw_datos
dw_lista dw_lista
cb_3 cb_3
cb_guardar cb_guardar
end type
global w_peronas w_peronas

on w_peronas.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.cb_insertar=create cb_insertar
this.dw_datos=create dw_datos
this.dw_lista=create dw_lista
this.cb_3=create cb_3
this.cb_guardar=create cb_guardar
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_insertar
this.Control[iCurrent+4]=this.dw_datos
this.Control[iCurrent+5]=this.dw_lista
this.Control[iCurrent+6]=this.cb_3
this.Control[iCurrent+7]=this.cb_guardar
end on

on w_peronas.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.cb_insertar)
destroy(this.dw_datos)
destroy(this.dw_lista)
destroy(this.cb_3)
destroy(this.cb_guardar)
end on

type cb_2 from commandbutton within w_peronas
integer x = 2304
integer y = 416
integer width = 329
integer height = 72
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ordenar"
end type

event clicked;string ls_sort
dw_lista.setsort("fname")
dw_lista.sort( )
end event

type cb_1 from commandbutton within w_peronas
integer x = 2304
integer y = 288
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Borrar"
end type

event clicked;dw_datos.deleterow(1)
cb_guardar.event clicked( )
end event

type cb_insertar from uo_boton within w_peronas
integer x = 2304
integer y = 160
integer taborder = 30
string text = "Insertar"
end type

event clicked;call super::clicked;long ll_id

dw_datos.reset()
dw_datos.insertrow(0)

select max(id)
   into :ll_id
  from customer;
  
 if sqlca.sqlcode < 0 then
	messagebox("ERROR",sqlca.sqlerrtext)
else
	ll_id ++
end if

dw_datos.setitem(1,1,ll_id)






end event

type dw_datos from uo_dw_base within w_peronas
integer x = 41
integer y = 716
integer taborder = 40
string dataobject = "dw_customer"
end type

type dw_lista from uo_dw_base within w_peronas
integer x = 37
integer y = 24
integer width = 2235
integer height = 652
integer taborder = 10
string dataobject = "dw_lista_customers"
boolean vscrollbar = true
boolean ib_autoretrieve = true
end type

event constructor;call super::constructor;this.setrowfocusindicator( hand!)
end event

event rowfocuschanged;call super::rowfocuschanged;long ll_id

ll_id = This.Getitemnumber(currentrow, 1)
//ll_id = This.Getitemnumber(currentrow, "id")

dw_datos.retrieve(ll_id)
end event

type cb_3 from uo_boton within w_peronas
integer x = 2299
integer y = 552
integer taborder = 30
string text = "Salir"
end type

event clicked;call super::clicked;close(parent)
end event

type cb_guardar from uo_boton within w_peronas
integer x = 2299
integer y = 36
integer taborder = 20
string text = "Guardar"
end type

event clicked;call super::clicked;integer li_ret

li_ret = dw_datos.update()

if li_ret = 1 then
	commit;
	dw_lista.retrieve( )
else 
	messagebox("ERROR",sqlca.sqlerrtext)
	rollback;
end if
	

end event

