﻿$PBExportHeader$w_reporte.srw
forward
global type w_reporte from w_padre
end type
type cb_3 from uo_boton within w_reporte
end type
type cb_2 from uo_boton within w_reporte
end type
type cb_1 from uo_boton within w_reporte
end type
end forward

global type w_reporte from w_padre
integer height = 2744
string title = "Personas"
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
end type
global w_reporte w_reporte

on w_reporte.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
end on

on w_reporte.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
end on

type cb_3 from uo_boton within w_reporte
integer x = 4133
integer y = 416
integer taborder = 30
string text = "Salir"
end type

type cb_2 from uo_boton within w_reporte
integer x = 4133
integer y = 232
integer taborder = 20
string text = "Imprimir"
end type

type cb_1 from uo_boton within w_reporte
integer x = 4133
integer y = 68
integer taborder = 10
string text = "Leer"
end type

