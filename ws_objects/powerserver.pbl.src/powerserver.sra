﻿$PBExportHeader$powerserver.sra
$PBExportComments$Generated Application Object
forward
global type powerserver from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
uo_vars gn_vars
end variables

global type powerserver from application
string appname = "powerserver"
string themepath = "C:\Program Files (x86)\Appeon\Shared\PowerBuilder\theme190"
string themename = "Do Not Use Themes"
long richtextedittype = 2
long richtexteditversion = 1
string richtexteditkey = ""
end type
global powerserver powerserver

type variables

end variables

on powerserver.create
appname="powerserver"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on powerserver.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;gn_vars = Create uo_vars
end event

event close;destroy gn_vars
open(w_logon)
end event

