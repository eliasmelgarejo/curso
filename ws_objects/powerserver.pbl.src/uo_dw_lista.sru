﻿$PBExportHeader$uo_dw_lista.sru
forward
global type uo_dw_lista from userobject
end type
type st_titulo from statictext within uo_dw_lista
end type
type cb_1 from uo_boton within uo_dw_lista
end type
type dw_1 from uo_dw_base within uo_dw_lista
end type
end forward

global type uo_dw_lista from userobject
integer width = 2999
integer height = 1956
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_volver ( )
st_titulo st_titulo
cb_1 cb_1
dw_1 dw_1
end type
global uo_dw_lista uo_dw_lista

forward prototypes
public subroutine uf_ver (string as_dw, string as_titulo)
end prototypes

public subroutine uf_ver (string as_dw, string as_titulo);dw_1.dataobject = as_dw
dw_1.settransobject(sqlca)

if dw_1.retrieve() > 0 then
	this.visible = true
	st_titulo.text = as_titulo
else
	messagebox("ERROR EN LISTA", sqlca.sqlerrtext)
	this.event ue_volver()
end if
	
end subroutine

on uo_dw_lista.create
this.st_titulo=create st_titulo
this.cb_1=create cb_1
this.dw_1=create dw_1
this.Control[]={this.st_titulo,&
this.cb_1,&
this.dw_1}
end on

on uo_dw_lista.destroy
destroy(this.st_titulo)
destroy(this.cb_1)
destroy(this.dw_1)
end on

type st_titulo from statictext within uo_dw_lista
integer x = 187
integer y = 96
integer width = 2254
integer height = 112
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 15780518
string text = "none"
boolean focusrectangle = false
end type

type cb_1 from uo_boton within uo_dw_lista
integer x = 2523
integer y = 84
integer taborder = 20
string text = "Volver"
end type

type dw_1 from uo_dw_base within uo_dw_lista
integer x = 101
integer y = 272
integer taborder = 10
end type

