﻿$PBExportHeader$uo_menu.sru
forward
global type uo_menu from userobject
end type
type pb_4 from uo_pb_button within uo_menu
end type
type pb_3 from uo_pb_button within uo_menu
end type
type pb_2 from uo_pb_button within uo_menu
end type
type pb_1 from uo_pb_button within uo_menu
end type
end forward

global type uo_menu from userobject
integer width = 690
integer height = 924
long backcolor = 15780518
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event type string a_opcion ( )
pb_4 pb_4
pb_3 pb_3
pb_2 pb_2
pb_1 pb_1
end type
global uo_menu uo_menu

on uo_menu.create
this.pb_4=create pb_4
this.pb_3=create pb_3
this.pb_2=create pb_2
this.pb_1=create pb_1
this.Control[]={this.pb_4,&
this.pb_3,&
this.pb_2,&
this.pb_1}
end on

on uo_menu.destroy
destroy(this.pb_4)
destroy(this.pb_3)
destroy(this.pb_2)
destroy(this.pb_1)
end on

type pb_4 from uo_pb_button within uo_menu
integer x = 133
integer y = 708
integer taborder = 40
string text = "Grafico"
end type

type pb_3 from uo_pb_button within uo_menu
integer x = 128
integer y = 532
integer taborder = 30
string text = "Persona"
end type

type pb_2 from uo_pb_button within uo_menu
integer x = 114
integer y = 344
integer taborder = 20
string text = "Estado"
end type

type pb_1 from uo_pb_button within uo_menu
integer x = 128
integer y = 168
integer taborder = 10
string text = "Productos"
end type

