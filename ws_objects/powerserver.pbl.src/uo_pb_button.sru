﻿$PBExportHeader$uo_pb_button.sru
forward
global type uo_pb_button from picturebutton
end type
end forward

global type uo_pb_button from picturebutton
integer width = 503
integer height = 156
integer transparency = 60
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\Users\SupervisorTI\Pictures\icons_png\64\sales_64x64.png"
vtextalign vtextalign = vcenter!
end type
global uo_pb_button uo_pb_button

on uo_pb_button.create
end on

on uo_pb_button.destroy
end on

event clicked;parent.dynamic event ue_ver_opcion(this.text)

end event

