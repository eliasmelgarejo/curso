﻿$PBExportHeader$uo_trans.sru
forward
global type uo_trans from transaction
end type
end forward

global type uo_trans from transaction
end type
global uo_trans uo_trans

type variables
boolean ib_connectl
end variables

forward prototypes
public function integer uf_connect (string as_user, string as_pwd)
end prototypes

public function integer uf_connect (string as_user, string as_pwd);// Profile EAS Demo DB V125
this.DBMS = "ODBC" //tipo de conexion a BD 
this.AutoCommit = False



if gn_vars.is_client_type = "PB" then	
	this.DBParm = "ConnectString='PB Demo DB V2019;UID="+as_user+";PWD=" + as_pwd +"'"
else
	this.logid = as_User
	this.logpass = as_pwd
	this.userid = as_user
	this.dbpass = as_pwd
end if
connect using this;

if this.sqlcode < 0 then 
	return -1
else 
	return 1
end if







end function

on uo_trans.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_trans.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

