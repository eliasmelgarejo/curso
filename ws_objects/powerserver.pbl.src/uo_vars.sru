﻿$PBExportHeader$uo_vars.sru
forward
global type uo_vars from nonvisualobject
end type
end forward

global type uo_vars from nonvisualobject
end type
global uo_vars uo_vars

type variables
string is_client_type
end variables
on uo_vars.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_vars.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;is_client_type = appeongetclienttype()
end event

