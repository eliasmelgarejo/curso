﻿$PBExportHeader$w_base.srw
forward
global type w_base from window
end type
end forward

global type w_base from window
integer width = 3803
integer height = 2088
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
end type
global w_base w_base

on w_base.create
end on

on w_base.destroy
end on

event open;if gn_vars.is_client_type <> "PB" then
	this.windowstate = maximized!
end if

end event

