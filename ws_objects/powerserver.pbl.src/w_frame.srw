﻿$PBExportHeader$w_frame.srw
forward
global type w_frame from w_base
end type
type uo_lista from uo_dw_lista within w_frame
end type
type p_banner from uo_banner2 within w_frame
end type
type uo_menu_win from uo_menu within w_frame
end type
type p_pie from uo_pie within w_frame
end type
end forward

global type w_frame from w_base
integer width = 4695
integer height = 2760
uo_lista uo_lista
p_banner p_banner
uo_menu_win uo_menu_win
p_pie p_pie
end type
global w_frame w_frame

on w_frame.create
int iCurrent
call super::create
this.uo_lista=create uo_lista
this.p_banner=create p_banner
this.uo_menu_win=create uo_menu_win
this.p_pie=create p_pie
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_lista
this.Control[iCurrent+2]=this.p_banner
this.Control[iCurrent+3]=this.uo_menu_win
this.Control[iCurrent+4]=this.p_pie
end on

on w_frame.destroy
call super::destroy
destroy(this.uo_lista)
destroy(this.p_banner)
destroy(this.uo_menu_win)
destroy(this.p_pie)
end on

event resize;call super::resize;p_pie.y = this.height - p_pie.height
p_pie.width = this.width

p_banner.x = (this.width / 2 ) - (p_banner.width / 2 )

uo_menu_win.height = this.height - uo_menu.y - p_pie.height
end event

type uo_lista from uo_dw_lista within w_frame
integer x = 1088
integer y = 352
integer width = 3104
integer height = 1620
integer taborder = 30
end type

on uo_lista.destroy
call uo_dw_lista::destroy
end on

event ue_volver;call super::ue_volver;uo_menu_win.enabled = false 
end event

type p_banner from uo_banner2 within w_frame
integer x = 1559
integer y = 28
integer height = 176
end type

type uo_menu_win from uo_menu within w_frame
event ue_ver_opcion ( string a_opcion )
integer x = 23
integer y = 720
integer width = 869
integer height = 1004
integer taborder = 20
end type

event ue_ver_opcion(string a_opcion);if a_opcion = "Productos" then
	uo_lista.uf_ver("dddw_producto","Lista Productos")
end if 

if a_opcion = "Estado" then
	uo_lista.uf_ver("dddw_states","Lista de Estados")
end if 

if a_opcion = "Cliente" then
	uo_lista.uf_ver("dw_lista_customers","Lista de Clientes")
end if 
end event

on uo_menu_win.destroy
call uo_menu::destroy
end on

type p_pie from uo_pie within w_frame
integer x = 1728
integer y = 2024
integer width = 2130
integer height = 484
boolean originalsize = false
end type

