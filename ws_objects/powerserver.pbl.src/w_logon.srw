﻿$PBExportHeader$w_logon.srw
forward
global type w_logon from w_base
end type
type p_banner from uo_banner2 within w_logon
end type
type p_pie from uo_pie within w_logon
end type
type cb_salir from uo_boton within w_logon
end type
type cb_login from uo_boton within w_logon
end type
type sle_pwd from singlelineedit within w_logon
end type
type sle_user from singlelineedit within w_logon
end type
type st_2 from statictext within w_logon
end type
type st_1 from statictext within w_logon
end type
end forward

global type w_logon from w_base
integer height = 2344
p_banner p_banner
p_pie p_pie
cb_salir cb_salir
cb_login cb_login
sle_pwd sle_pwd
sle_user sle_user
st_2 st_2
st_1 st_1
end type
global w_logon w_logon

on w_logon.create
int iCurrent
call super::create
this.p_banner=create p_banner
this.p_pie=create p_pie
this.cb_salir=create cb_salir
this.cb_login=create cb_login
this.sle_pwd=create sle_pwd
this.sle_user=create sle_user
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.p_banner
this.Control[iCurrent+2]=this.p_pie
this.Control[iCurrent+3]=this.cb_salir
this.Control[iCurrent+4]=this.cb_login
this.Control[iCurrent+5]=this.sle_pwd
this.Control[iCurrent+6]=this.sle_user
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.st_1
end on

on w_logon.destroy
call super::destroy
destroy(this.p_banner)
destroy(this.p_pie)
destroy(this.cb_salir)
destroy(this.cb_login)
destroy(this.sle_pwd)
destroy(this.sle_user)
destroy(this.st_2)
destroy(this.st_1)
end on

event resize;call super::resize;p_pie.y = this.height - p_pie.height
p_pie.width = this.width

p_banner.x = (this.width / 2) - (p_banner.width / 2)
end event

type p_banner from uo_banner2 within w_logon
integer x = 837
integer y = 64
end type

type p_pie from uo_pie within w_logon
integer x = 169
integer y = 1380
integer width = 3035
integer height = 716
boolean originalsize = false
end type

type cb_salir from uo_boton within w_logon
integer x = 1746
integer y = 1124
integer taborder = 40
string text = "Salir"
end type

type cb_login from uo_boton within w_logon
integer x = 850
integer y = 1120
integer taborder = 30
string text = "Aceptar"
end type

event clicked;call super::clicked;/*if sqlca.uf_connect( sle_user.text, sle_pwd.text) = -1 then
	messagebox("ERROR","sqlca.sqlerrtext")
	disconnect using sqlca;
end if
*/
open(w_frame)
//close(parent)
end event

type sle_pwd from singlelineedit within w_logon
integer x = 1070
integer y = 660
integer width = 1239
integer height = 120
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean password = true
borderstyle borderstyle = stylelowered!
end type

type sle_user from singlelineedit within w_logon
integer x = 1065
integer y = 500
integer width = 1248
integer height = 136
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_logon
integer x = 617
integer y = 696
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "CONTRASEÑA"
boolean focusrectangle = false
end type

type st_1 from statictext within w_logon
integer x = 617
integer y = 528
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "USUARIO"
boolean focusrectangle = false
end type

