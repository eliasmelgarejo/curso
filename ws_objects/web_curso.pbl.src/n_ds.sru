﻿$PBExportHeader$n_ds.sru
forward
global type n_ds from datastore
end type
end forward

global type n_ds from datastore
end type
global n_ds n_ds

forward prototypes
public subroutine of_set_dataobject (string as_do)
public subroutine of_set_dataobject (string as_do, boolean ab_retrieve)
end prototypes

public subroutine of_set_dataobject (string as_do);this.dataobject = as_do
this.settransobject(sqlca)
end subroutine

public subroutine of_set_dataobject (string as_do, boolean ab_retrieve);this.of_set_dataobject(as_do)

if ab_retrieve then this.retrieve()
end subroutine

on n_ds.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_ds.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

