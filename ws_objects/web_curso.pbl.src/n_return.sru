﻿$PBExportHeader$n_return.sru
forward
global type n_return from nonvisualobject
end type
end forward

global type n_return from nonvisualobject autoinstantiate
end type

type variables
string is_ret_str
integer ii_ret_code
string is_ret_data

end variables
on n_return.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_return.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

