﻿$PBExportHeader$n_web_curso.sru
$PBExportComments$Generated Web service object
forward
global type n_web_curso from nonvisualobject
end type
end forward

global type n_web_curso from nonvisualobject descriptor "PB_ObjectCodeAssistants" = "{1E00F051-675A-11D2-BCA5-000086095DDA}" 
end type
global n_web_curso n_web_curso

forward prototypes
public function long f_sum (long a_val1, long a_val2)
public subroutine f_interna ()
public function n_return of_connect ()
public function n_return f_get_productos ()
end prototypes

public function long f_sum (long a_val1, long a_val2);return a_val1 + a_val2
end function

public subroutine f_interna ();int ii_val = 1
end subroutine

public function n_return of_connect ();// Profile PB Demo DB V2019
n_return ln_ret


SQLCA.DBMS = "ODBC"
SQLCA.AutoCommit = False
SQLCA.DBParm = "ConnectString='DSN=PB Demo DB V2019;UID=dba;PWD=sql'"

connect using sqlca;

if sqlca.sqlcode = 0 then
	ln_ret.is_ret_str = "OK"
	ln_ret.ii_ret_code = 1
else
	ln_ret.is_ret_str = "NO OK"
	ln_ret.ii_ret_code = sqlca.sqlcode
	ln_ret.is_ret_data=sqlca.sqlerrtext
end if
	
return ln_ret


end function

public function n_return f_get_productos ();n_return ln_ret
n_ds lds_data

ln_ret = this.of_connect()

if ln_ret.ii_ret_code = 1 then
	lds_data = create n_ds
	lds_data.of_set_dataobject("dw_product", true)
	
	if lds_data.rowcount() > 0 then
		ln_ret.is_ret_str = "LISTA PRODUCTOS"
		ln_ret.is_ret_data = lds_data.Object.DataWindow.Data.XML
	else
		ln_ret.is_ret_str = "ERROR EN LISTA PRODUCTOS"
		ln_ret.ii_ret_code = 100
	end if
end if

return ln_ret
end function

on n_web_curso.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_web_curso.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

