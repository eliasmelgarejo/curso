﻿$PBExportHeader$web_curso.sra
$PBExportComments$Generated Application Object
forward
global type web_curso from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type web_curso from application
string appname = "web_curso"
end type
global web_curso web_curso

on web_curso.create
appname = "web_curso"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on web_curso.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

